<div class="left_panel panel">
	<form id="contatti_form">
	    <input type="hidden" id="token" value="{{ csrf_token() }}" name="_token">
	    <ul>
	        <li>
	        	<input class="input" name="name" type="text" data-msg-required="Non hai inserito il tuo nome" data-rule-required="true" value="" data-rule-notEqual="Nome" data-msg-notEqual="Non hai ancora inserito il tuo nome">
	        </li>
	        <li>
	        	<input class="input" name="lastname" type="text" data-msg-required="Non hai inserito il tuo cognome" data-rule-required="true" value="" data-rule-notEqual="Cognome" data-msg-notEqual="Inserisci un cognome per favore">
	        </li>
	        <li>
	        	<input class="input" name="email" type="text" data-msg-email="Mi dispaice, l'e-mail che hai inserito non ci risulta valida" data-msg-required="Hai dimenticato di inserire un indirizzo e-mail" data-rule-required="true" value="" data-rule-notEqual="E-Mail" data-rule-email="true">
	        </li>
	        <li>
	        	<input class="input" name="phone" type="text" data-rule-regex="^([0-9]*\-?\ ?\/?[0-9]*)$" data-msg-regex="Non sembra un numero di telefono valido" data-msg-digits="" data-msg-required="Vorremmo un tuo numero di telefono per contattarti" data-rule-required="true" value="" data-rule-notEqual="Telefono" data-msg-notEqual="Dovresti fornirci un numero di telefono, grazie">
	        </li>
	        <li>
	        	<textarea class="input" name="textarea" class="data-rule-notEqualText" data-msg-required="Il messaggio non può essere vuoto" data-rule-required="true" data-rule-notEqual="Messaggio" data-msg-notEqual="'Messaggio' non è un messaggio accettabile ;)" data-rule-minlength="10" data-msg-minlength="Il messaggio è troppo corto, scrivici qualcosa di più"></textarea>
	        </li>
	        <li>
	        	<div id="errorContainer"><div class="text"></div></div>
	        	<input class="button" id="invia" type="submit" value="invia">
	        </li>
	    </ul>
	</form>
</div>
<div class="right_panel panel">
	<table id="contatti_details">
		<tr>
			<td>
				<span class="fa fa-phone fa-lg"></span>
			</td>
			<td class="right_column">
				389.9138801 - 348.4730018
			</td>
		</tr>
		<tr>
			<td>
				<span class="fa fa-envelope fa-lg"></span>
			</td>
			<td class="right_column">
				info @ balletstudioseveso . dance
			</td>
		</tr>
		<tr>
			<td>
				<span class="fa fa-map-marker fa-lg"></span>
			</td>
			<td class="right_column">
				<a href="https://goo.gl/maps/NyTAq" target="_gmap">Via Colleoni 4, Seveso</a>
			</td>
		</tr>
		<tr>
			<td>
				<span class="fa fa-facebook fa-lg"></span>
			</td>
			<td class="right_column">
				<a href="http://www.facebook.com/ScuolaDanzaBalletStudio" target="_blank">Facebook</a>
			</td>
		</tr>
		<tr>
			<td>
				<span class="fa fa-instagram fa-lg"></span>
			</td>
			<td class="right_column">
				<a href="https://instagram.com/scuola_danza_ballet_studio_/" target="_instangram">Instagram</a>
			</td>
		</tr>
	</table>
<script type="text/javascript">
	function reset_form() {
		$('.input', $('#contatti_form')).each(function(){
			$(this).val($(this).attr('data-rule-notEqual'));
		});
	}

	function contatti_sizer(elements){
		$('#contatti_details td').css({
		    height: elements.content.height() / $('#contatti_details tr').size()
		});
	}

	contatti_sizer(elements);
	$(window).resize(function() {
        contatti_sizer(elements);
    });

    reset_form();

	$('.input', $('#contatti_form')).click(function(){
		var old_focus = $('.input_focus').first();
		if(old_focus.val() == '') {
			old_focus.val(old_focus.attr('data-rule-notEqual'));
		}
		
		old_focus.removeClass('input_focus');

		var new_focus = $(this);
		new_focus.addClass('input_focus');

		if(new_focus.val() == new_focus.attr('data-rule-notEqual')) {
			new_focus.val('');
		}
	});

	var found = false;
	$('#contatti_form').validate({
		onkeyup: false,
		onclick: false,
		showErrors: function(errorMap, errorList) {
			if(errorList[0]) {
				var first_error = errorList[0].message;
				$('#errorContainer .text').html(first_error);
			}
	    },
	    submitHandler: function(form) {
	        var url = 'function/sendMessage'; // the script where you handle the form input.
	        var values = $(form).serialize();
	        
	        $.ajax({
	            type: 'POST',
	            url: url,
	            data: values, // serializes the form's elements.
	            dataType: 'json',
	            beforeSend: function(){
	                $.msg({
	                    msgID: 1,
	                    autoUnblock: false,
	                    clickUnblock: false,
	                    content: '<span class="cssload-loader"><span class="cssload-loader-inner"></span></span> Attendi solo qualche secondo, grazie'
	                });
	            },
	            success: function(data){
	                $.msg('replace', data['message']);
	                $.msg('unblock', 3500, 1 );
	            },
	            error: function(jqXHR, textStatus, errorThrown ) {
	            	console.log(jqXHR);
	            	console.log(textStatus);
	            	console.log(errorThrown);
	            	$.msg('replace', 'errore :(');
	            	$.msg('unblock', 1500, 1 );
	            }
	        });
	    }
	});

	jQuery.validator.addMethod("notEqual", function(value, element, param) {
		return this.optional(element) || value != param;
	}, "Please specify a different (non-default) value");

	$.validator.addMethod(
        "regex",
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        },
        "Please check your input."
	);
</script>