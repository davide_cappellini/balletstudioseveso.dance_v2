<div style="background-image: url(img/logo-ballet_199x79.png); width: 199px; height: 79px; margin-bottom: 5px;"></div>
<div style="background-image: url(img/borderBallet_vertical.png); height: 4px; width: 199px;"></div>
<table style="font-size: 15px;">
    <tr>
        <td><span style="font-weight: bold;text-transform: capitalize;">Nome:</span></td>
        <td>{{$name}}</td>
    </tr>
    <tr>
        <td><span style="font-weight: bold;text-transform: capitalize;">Cognome:</span></td>
        <td>{{$lastname}}</td>
    </tr>
    <tr>
        <td><span style="font-weight: bold;text-transform: capitalize;">E-mail:</span> </td>
        <td>{{$email}}</td>
    </tr>
    <tr>
        <td><span style="font-weight: bold;text-transform: capitalize;">Telefono:</span></td>
        <td>{{$phone}}</td>
    </tr>
</table>
<div style="font-style: italic; font-size: 15px;">{{$textarea}}</div> 
