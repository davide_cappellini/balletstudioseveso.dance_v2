<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/vendor/normalize.css">
        <link rel="stylesheet" href="css/app.css">
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div id="tiling">
            <div class="tile" style="opacity: 0;"></div>
            <div class="tile" style="opacity: 0.3;"></div>
            <div class="tile" style="opacity: 0.1;"></div>
            <div class="tile" style="opacity: 0.6;"></div>
            <div class="tile" style="opacity: 0.4;"></div>
            <div class="tile" style="opacity: 0;"></div>
            <div class="tile" style="opacity: 0.2;"></div>
            <div class="tile" style="opacity: 0.4;"></div>
        </div>

        <div id="menu">
            <img src="img/logo-ballet_white.svg" id="logo">
            <div id="links">
                <div class="link" view="contatti">contatti</div>
                <div class="link" view="orario" id="clickme">orario</div>
                <div class="link" view="home">home</div>
            </div>
        </div>

        <div id="content">
            <div  id="front" class="side shadow_right"></div>
            <div id="back" class="side shadow_right"></div>
        </div>

        <footer>
            informativa cookies bla bla bla
        </footer>

        <script type="text/javascript">@yield('script')</script>
        <script src="js/vendor.js"></script>
        <script src="js/app.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
