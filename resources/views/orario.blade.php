<h2 class="page_title">Orario corsi 2015/2016</h2>
<div id="about" class="nano">
    <div class="nano-content">
      <table id="table_orari">
         <thead>
            <tr>
               <th><div class="nome_corso">nome del corso</div></th>
               <th><div class="giorno">giorno</div></th>
               <th><div class="orario">orario</div></th>
               <!-- <th><div class="orario">alle</div></th> -->
               <th><div class="note">note</div></th>
            </tr>
         </thead>
         <tbody id="loadHere">
            <!-- @for ($i = 0; $i < 50; $i++)
                <tr>
                  <td>
                     <div class="nome_corso">
                        blablabla {{ $i }}
                     </div>
                  </td>
                  <td>
                     <div class="giorno">
                        blablabla
                     </div>
                  </td>
                  <td>
                     <div class="orario">
                        16:00
                     </div>
                  </td>
                  <td>
                     <div class="orario">
                        16:00
                     </div>
                  </td>
                </tr>
            @endfor -->
         </tbody>
      </table>
    </div>
</div>
<div class="fa fa-angle-up fa-lg arrow"></div>
<div class="fa fa-angle-down fa-lg arrow"></div>
<div class="page_footer"><p class="fa fa-print"></p>&nbsp;<a href="/pdf/OrarioBalletStudio2015_2016.pdf" target="_orario">Versione stampabile (PDF)</a></div>
<script type="text/javascript">
   var nice = false;
   function corsi_sizer(elements){
      var max_width = $('.nano-content').first().width();
      $('.nome_corso').css({
         width: max_width * 0.27
      });

      $('.giorno').css({
         width: max_width * 0.13
      });

      $('.orario').css({
         width: max_width * 0.20
      });

      $('.note').css({
         width: max_width * 0.30
      });

      $('.page_title').css({
         width: $('#table_orari').outerWidth(),
         marginLeft: ( elements.content.outerWidth() - $('#table_orari').outerWidth() ) / 2 - 5
      });

   }

   function leadZero(x) {
      return ('0' + x).slice(-2);
   }

   function insertOrari(orari) {
      for (var i = orari.length - 1; i >= 0; i--) {
         var inizio = orari[i].inizio[0]+':'+leadZero(orari[i].inizio[1]);
         var fine = orari[i].fine[0]+':'+leadZero(orari[i].fine[1]);
         var dalle_alle = 'dalle ' + inizio + ' alle ' + fine
         var note = orari[i].note == null ? '&nbsp;' : orari[i].note;
         // var row = '<tr ztyle="text-shadow: 1px 1px '+orari[i].color+';"><td><div class="nome_corso">'+orari[i].corso+'</div></td><td><div class="giorno">'+orari[i].giorno+'</div></td><td><div class="orario">'+inizio+'</div></td><td><div class="orario">'+fine+'</div></td><td><div class="note">'+note+'</div></td></tr>';
         var row = '<tr ztyle="text-shadow: 1px 1px '+orari[i].color+';"><td><div class="nome_corso">'+orari[i].corso+'</div></td><td><div class="giorno">'+orari[i].giorno+'</div></td><td><div class="orario">'+dalle_alle+'</div></td><td><div class="note">'+note+'</div></td></tr>';

          $('#loadHere').prepend(row);
      };

      corsi_sizer(elements);
      $('table#table_orari').tablesorter().fadeIn();
      $('.page_title').fadeIn();
   }

   var url = 'https://docs.google.com/spreadsheets/d/1qDz7JaymzoA9_MW8Bb87lNZMBvoaVoalcu8G7PJep8I/edit?usp=sharing';
   var query = "SELECT A,B,C,D,E,F ORDER BY A";
   function queryGoogle() {
      blockspring.runParsed("query-google-spreadsheet", {
         "query": query,
         "url": url
      }, {
         "api_key": "br_16438_a99ca675cc993e0789c38f7d82069126aeca6761"
      }, function(res){

         insertOrari(res.params.data);

         $('div', $('tr:last', $('#table_orari'))).css({
            borderBottom: 0
         });

         $(".nano").nanoScroller({
            alwaysVisible: true
         });
      });
   }

   $(document).ready(function(){
      queryGoogle();

      corsi_sizer(elements);
      $(window).resize(function() {
         corsi_sizer(elements);
      });
   });
</script>