var background_images = [
        'img/breve-storia-della-danza-classica_eb030924c091fe5c27e30db081663469.jpg'
        ];
var ratio = 1.7777;

var elements = {};

function sizer(elements) {
    var window_width = $(window).width();
    var window_height = $(window).height();

    if(window_width < 1024) window_width = 1024;
    if(window_height < 768) window_height = 768;
	elements.body.css({
		width: window_width,
		height: window_height
	});

    var percent = 0.4;
    var content_width = window_width * percent;
    if(content_width < 792) content_width = 792;
    var content_left = (window_width - content_width) / 2;
    var content_height = content_width / ratio;
    var content_top = (window_height - content_height ) /2;
    elements.content.css({
        left: content_left,
        top: content_top,
        width: content_width,
        height: content_height
    });

    elements.logo.css({
        left: content_left
    });

    var logo_width = 242;
    var links_width = content_width - logo_width;
    elements.links.css({
        width: links_width,
        marginLeft: content_left + logo_width
    });

    var menu_height = window_height * 0.05;
    elements.menu.css({
        height: menu_height
    });

    $('.link').css({
        width: '30%',
        lineHeight: menu_height + 'px'
    });
}

$(document).ready(function() {
    elements = {
        body:$('body'),
        menu:$('#menu'),
        logo:$('#logo'),
        content:$('#content'),
        links:$('#links'),
        front:$('#front'),
        back:$('#back')
    };

    var front = false;
    var target = elements.back;
    var remove = elements.front;

    elements.content.fadeOut(0);

	sizer(elements);
	$(window).resize(function() {
        sizer(elements);
    });

    elements.body.backstretch(background_images).backstretch('pause');

    elements.content.flip({
        trigger: 'manual',
        front: elements.front,
        back: elements.back
    });

    $('.link').click(function(){
        if($(this).hasClass('selected')) return;

        $('.link').removeClass('selected');
        $(this).addClass('selected');

        var view = '/'+$(this).attr('view');

        $.get(view, function(html){
            target.html(html);
            elements.content.flip('toggle');
            remove.html('');

            if(front) {
                target = elements.back;
                remove = elements.front;
            }
            else {
                target = elements.front;
                remove = elements.back;
            }

            front = !front;
        })

        if(!elements.content.is(':visible')){
            elements.content.fadeIn();
        }
    });

    $('#clickme').click();
});