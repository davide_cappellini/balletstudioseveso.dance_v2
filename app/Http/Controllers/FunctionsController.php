<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Mail;


class FunctionsController extends Controller {

    public function sendMessage(Request $request) {
        $data = [
            'name' => $request->input('name'),
            'lastname' => $request->input('lastname'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'textarea' => $request->input('textarea')
        ];

        Mail::queue('emails.sendMessage', $data, function($message) use ($data){
            $message->from('contatti@local.balletstudioseveso.dance', 'Notifica Contatti')
                    // ->to('info@balletstudioseveso.dance', 'Ballet Studio')
                    ->to('signorsasso@gmail.com', 'Davide it')
                    ->bcc('davide.cappe.uk@gmail.com', 'Davide uk')
                    ->replyTo($data['email'], $data['name'].' '.$data['lastname'])
                    ->subject('Nuovo messaggio');
        });

        return array('result'=>'1', 'message'=>'Messaggio inviato con successo, grazie per averci contatto!');
        //return array('result'=>1, 'message'=>substr($request->root(), 7));
    }

    public function queryGoogle() {
        return array('result'=>'1', 'message'=>$query);
    }

}