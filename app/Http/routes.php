<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');

Route::get('/home', 'MainController@home');
Route::get('/orario', 'MainController@orario');
Route::get('/contatti', 'MainController@contatti');

Route::resource('function/sendMessage', 'FunctionsController@sendMessage');
Route::get('function/queryGoogle/{query}', 'FunctionsController@queryGoogle');
