var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

var elixir = require('laravel-elixir');
 
var bowerDir = './resources/assets/vendor/';
 
var lessPaths = [
    bowerDir + "font-awesome/less",
    bowerDir + "jquery.msg",
    bowerDir + "pwstabs/assets",
    bowerDir + "nanoscroller/bin/css",
    bowerDir + "tablesorter/dist/css/less"
];
 
elixir(function(mix) {
    mix.less('app.less', 'public/css', { paths: lessPaths })
        .copy('resources/assets/vendor/normalize-css/normalize.css', 'public/css/vendor/normalize.css')
    	.scripts([
            'jquery/dist/jquery.min.js',
            'jquery.transit/jquery.transit.js',
            'flip/dist/jquery.flip.min.js',
            'jquery-backstretch/src/jquery.backstretch.js',
            'jquery-validation/dist/jquery.validate.min.js',
            'jquery.msg/jquery.center.min.js',
            'jquery.msg/jquery.msg.min.js',
            'pwstabs/assets/jquery.pwstabs.min.js',
            'nanoscroller/bin/javascripts/jquery.nanoscroller.min.js',
            'blockspring-js/index.js',
            'tablesorter/dist/js/jquery.tablesorter.min.js'
            ], 'public/js/vendor.js', bowerDir)
        .copy('resources/assets/js/app.js', 'public/js/app.js')
        .copy(bowerDir + 'font-awesome/fonts', 'public/fonts');
 
});